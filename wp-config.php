<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'voxconnector');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd}MQo9!hl.osUpi]W:+?Ydmf8S$sRq2qhmK))5URzx@i=m(;m-!kjUF?YS-%Y4NM');
define('SECURE_AUTH_KEY',  'Wd!oK(NAHf;eobE5-= eAmq{uh65.qH1W>rp}W9z?vrPt4c~o.*F=@ISlQ>B B!}');
define('LOGGED_IN_KEY',    'CzQ~0;=9nRD:l3+7gA-{ensQm=kb^I{^nkE0E%#FgFl0nryib&!vH*B[C1|6)Gaf');
define('NONCE_KEY',        ' albP2|d6@XDw^$iNr-/gU]Jw}aGM_oX;`$P/VAn297;?W~CD|fO-3yH_]5Lze)b');
define('AUTH_SALT',        'AK,PPQ7BsNw^Asi*9UpzdloA8Kd|T&:uKVzvz#3QMra@oL|f2i88o32a-VU*j=9x');
define('SECURE_AUTH_SALT', 'W7 !q!y%1{2*xyfUya|m(?e#{iD1BeQ^&G{!3:H^@j>#:gtSM*C(`r<<C9E_)]$1');
define('LOGGED_IN_SALT',   '5~s{e|S<>h;xfxO=!~qQu~5XE-F{?!bDwew@Dj${x_(wFkAB&2um))pcp,`I9!:H');
define('NONCE_SALT',       '+%i .#&Z5cS+e.)s533XN/Nm.{%n`E}U:HB]:Q6Zj!JS8r#&p?qlRcD<;X1MOyic');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
