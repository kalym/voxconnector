<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Dhii\\Stats\\' => array($vendorDir . '/dhii/stats-interface/src', $vendorDir . '/dhii/stats-abstract/src'),
    'Dhii\\Collection\\' => array($vendorDir . '/dhii/collections-interface/src', $vendorDir . '/dhii/collections-abstract-base/src', $vendorDir . '/dhii/collections-abstract/src'),
);
