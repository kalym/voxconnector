<?php

//require_once get_stylesheet_directory_uri() . '/inc/widgets/tm-news-smart-box-widget_new/class-tm-news-smart-box-widget_new.php';

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_stylesheet_directory_uri().'/style.css' );
}
function vox_register_time_post_type_metabox()
{

    $cmb = new_cmb2_box(array(
        'id' => 'time_metabox',
        'title' => __('Час', 'vox'),
        'object_types' => array('post'), // post type
        'context' => 'normal', //  'normal', 'advanced', or 'side'
        'priority' => 'high',  //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
    ));

    $cmb->add_field(array(
        'name' => __('Час на читання'),
        'id' => 'time_to_read',
        'type' => 'text',
        'default' => '3',
    ));

    $cmb->add_field( array(
        'name'    => __( 'Читайте також' ),
        'id'      => 'vox_related_posts',
        'type'    => 'custom_attached_posts',
        'options' => array(
            'show_thumbnails' => true, // Show thumbnails on the left
            'filter_boxes'    => true, // Show a text box for filtering the results
            'query_args'      => array(
                'post_type' => 'post',
                'posts_per_page' => -1 ),

            )
    ) );

}

add_action('cmb2_admin_init', 'vox_register_time_post_type_metabox');

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

    <table class="form-table">
        <tr>
            <th><label for="information">Коротка інформація</label></th>
            <td>
                <input type="text" name="information" id="information" value="<?php echo esc_attr( get_the_author_meta( 'information', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>

<?php }
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
    update_usermeta( $user_id, 'information', $_POST['information'] );
}
add_filter( 'avatar_defaults', 'customgravatar' );
add_filter( 'get_avatar' , 'my_custom_avatar' , 1 , 5 );

function my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
    $user = false;

    if ( is_numeric( $id_or_email ) ) {

        $id = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );
    }

    if ( $user && is_object( $user ) ) {

        if ( $user->data->ID == '1' ) {
            $avatar = 'YOUR_NEW_IMAGE_URL';
            $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
        }
    }

    return $avatar;
}


add_action( 'init', 'create_taxonomies', 0 );

function create_taxonomies() {
    $labels = array(
        'name'              => _x( 'Спеціальні', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Спеціальні', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Пошук Спеціальних', 'textdomain' ),
        'all_items'         => __( 'Спеціальні', 'textdomain' ),
//        'parent_item'       => __( 'Parent Genre', 'textdomain' ),
//        'parent_item_colon' => __( 'Parent Genre:', 'textdomain' ),
//        'edit_item'         => __( 'Edit Genre', 'textdomain' ),
//        'update_item'       => __( 'Update Genre', 'textdomain' ),
//        'add_new_item'      => __( 'Add New Genre', 'textdomain' ),
//        'new_item_name'     => __( 'New Genre Name', 'textdomain' ),
//        'menu_name'         => __( 'Genre', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'special' ),
    );
    register_taxonomy( 'post_special_category', array( 'post' ), $args );

    $labels = array(
        'name'              => _x( 'Теми', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Теми', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Пошук Тем', 'textdomain' ),
        'all_items'         => __( 'Теми', 'textdomain' ),
//        'parent_item'       => __( 'Parent Genre', 'textdomain' ),
//        'parent_item_colon' => __( 'Parent Genre:', 'textdomain' ),
//        'edit_item'         => __( 'Edit Genre', 'textdomain' ),
//        'update_item'       => __( 'Update Genre', 'textdomain' ),
//        'add_new_item'      => __( 'Add New Genre', 'textdomain' ),
//        'new_item_name'     => __( 'New Genre Name', 'textdomain' ),
//        'menu_name'         => __( 'Genre', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'themas' ),
    );
    register_taxonomy( 'post_themas_category', array( 'post' ), $args );

    $labels = array(
        'name'              => _x( 'Фокус', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Фокус', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Пошук Фокусів', 'textdomain' ),
        'all_items'         => __( 'Фокуси', 'textdomain' ),
//        'parent_item'       => __( 'Parent Genre', 'textdomain' ),
//        'parent_item_colon' => __( 'Parent Genre:', 'textdomain' ),
//        'edit_item'         => __( 'Edit Genre', 'textdomain' ),
//        'update_item'       => __( 'Update Genre', 'textdomain' ),
//        'add_new_item'      => __( 'Add New Genre', 'textdomain' ),
//        'new_item_name'     => __( 'New Genre Name', 'textdomain' ),
//        'menu_name'         => __( 'Genre', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'focus' ),
    );
    register_taxonomy( 'post_focus_category', array( 'post' ), $args );
}

add_action('customize_register', 'theme_vox_customizer');
function theme_vox_customizer($wp_customize)
{
    $wp_customize->add_section('vox_section', array(
        'title' => 'Застереження'
    ));
    $wp_customize->add_setting('text_setting', array(
        'default' => 'Введіть застереження',
    ));
    $wp_customize->add_control('text_setting', array(
        'label' => 'Текст',
        'section' => 'vox_section',
        'type' => 'textarea',
    ));
}
function display_taxonomy_terms($post_type, $display = false) {
    global $post;
    $term_list = wp_get_post_terms($post->ID, $post_type, array('fields' => 'names'));

    if($display == false) {
        echo $term_list[0];
    }elseif($display == 'return') {
        return  $term_list[0];
    }
}



function create_post_type()
{
    register_post_type('vox_index',
        array(
            'label' => __('Iндекс'),
//            'taxonomies' => array('news_category'),
            'public' => true,
            'has_archive' => true,
            'supports' => array(),
        )
    );
}
add_action('init', 'create_post_type');



function get_single_trimed_content( $content = '', $excerpt_length = 40 ) {
    $trimed_content = strip_shortcodes( $content );
    $trimed_content = apply_filters( 'the_content', $trimed_content );
    $trimed_content = str_replace(']]>', ']]&gt;', $trimed_content );
    $trimed_content = wp_trim_words( $trimed_content, $excerpt_length );

    return $trimed_content;
}
function new_excerpt_length($length) {
    return 400;
}
add_filter('excerpt_length', 'new_excerpt_length');



if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors_posts_links();
} else {
    the_author_posts_link();
}

