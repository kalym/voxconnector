<?php
/**
 * Template Name: custom
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package king_news
 */
while ( have_posts() ) : the_post();

    /*
     * Include the Post-Format-specific template for the content.
     * If you want to override this in a child theme, then include a file
     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
     */

    //if need different post templates for grid and masonry layouts
    $layout = get_theme_mod( 'blog_layout_type', 'default' );


    switch ( $layout ) {
        case 'grid-2-cols':
        case 'grid-3-cols':
            $layout = 'grid';
            break;

        case 'masonry-2-cols':
        case 'masonry-3-cols':
            $layout = 'masonry';
            break;

        case 'minimal':
            $layout = 'minimal';
            break;

        case 'default':
            $layout = 'default';
            break;
    }

    if( is_category( ) ){
        $layout = 'default';
    }

    $format = get_post_format();

    if ( in_array( $layout, array( 'default', 'minimal', 'masonry', 'grid' ) ) ) {
        if ( $format ) {
            $layout .= '-' . $format;
        }
    }

    get_template_part( 'template-parts/content', $layout );

endwhile; ?>

