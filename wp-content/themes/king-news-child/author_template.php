<?php
/**
 * Template Name: Author Template
 */
while ( have_posts() ) : the_post();
?>
    <div id="users" class="authors_block">
        <div class="search_author">
            <div class="author_list_titlе">Пошук по імені</div>

            <input type="text" class="search" placeholder="Пошук" />
        </div>
        <div class="alphabet_search">
            <ul>
                <li><a href="#">А</a></li>
                <li><a href="#">б</a></li>
                <li><a href="#">в</a></li>
                <li><a href="#">г</a></li>
                <li><a href="#">ґ</a></li>
                <li><a href="#">д</a></li>
                <li><a href="#">е</a></li>
                <li><a href="#">є</a></li>
                <li><a href="#">ж</a></li>
                <li><a href="#">з</a></li>
                <li><a href="#">и</a></li>
                <li><a href="#">і</a></li>
                <li><a href="#">ї</a></li>
                <li><a href="#">й</a></li>
                <li><a href="#">к</a></li>
                <li><a href="#">л</a></li>
                <li><a href="#">м</a></li>
                <li><a href="#">н</a></li>
                <li><a href="#">о</a></li>
                <li><a href="#">п</a></li>
                <li><a href="#">р</a></li>
                <li><a href="#">с</a></li>
                <li><a href="#">т</a></li>
                <li><a href="#">у</a></li>
                <li><a href="#">ф</a></li>
                <li><a href="#">ч</a></li>
                <li><a href="#">ц</a></li>
                <li><a href="#">ч</a></li>
                <li><a href="#">ш</a></li>
                <li><a href="#">щ</a></li>
                <li><a href="#">ь</a></li>
                <li><a href="#">ю</a></li>
                <li><a href="#">я</a></li>
            </ul>
        </div>

        <p class="no-result">Автора не знайдено</p>
        <?php
        $authors=get_users('');
        foreach($authors as $author){

            $authorList[$i]['id']=$author->data->ID;
            $authorList[$i]['name']=$author->data->display_name;
            $i++;
        }
        ?>

        <ul class="list">
            <?php
            foreach($authorList as $author){
                $args=array(
                    'showposts'=>1,
                    'author'=>$author['id'],
                    'caller_get_posts'=>1
                );
                $query = new WP_Query($args);
                if($query->have_posts() ) {
                    while ($query->have_posts()){
                        $query->the_post();
                        ?>
                        <li>
                            <a href="<?php echo get_author_posts_url( $author['id'] );?>" class="name"><?php echo $author['name']; ?></a>

                        </li>
                        <?php
                    }
                    wp_reset_postdata();
                }
            }
            ?>
        </ul>

        <ul class="pagination"></ul>

        <button id="updateList" class="updateList">Усі автори</button>
    </div>
<?php endwhile; ?>

