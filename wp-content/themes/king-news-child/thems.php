<?php
/**
 * Template Name: Thems
 */
?>
<div class="taxonomy_block">
    <ul class="list">
        <?php $categories = get_categories('taxonomy=post_themas_category');
        foreach ($categories as $category) : ?>
            <li><a href="<?php echo get_category_link($category->cat_ID); ?>"><?php echo $category->name; ?></a></li>
        <?php endforeach; ?>
     </ul>
</div>
