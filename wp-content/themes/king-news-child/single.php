<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package king_news
 */

while ( have_posts() ) : the_post();

	?><div class="post-left-column"><?php
		king_news_share_buttons( 'single' );
	?></div><div class="post-right-column"><?php
		get_template_part( 'template-parts/content', 'single' );

	?></div>
	<?php
	$currentPostId = get_the_ID();
	$relatedPostId = '';
	$posttags = get_the_tags();

	$term_list = wp_get_post_terms(get_the_ID(), 'category');
	if ( $term_list ) {
		$current_category = $term_list[0]->slug;
	}

	$attached = get_post_meta( get_the_ID(), 'vox_related_posts', true );
	if ($attached) {
		foreach ($attached as $attached_post) {
			$post = get_post($attached_post);
			setup_postdata($post);

			$relatedPostId = $post->ID;
		}
	} else {
		$query = new WP_Query( array(
			'category_name' => $current_category,
			'post__not_in' => array( $currentPostId ),
			'posts_per_page' => 1,
			'orderby' => 'rand'
		) );
		if ( $query->have_posts() ) {
			$query->the_post();

			$relatedPostId = get_the_ID();
		}
	}

	$timeToRead = get_post_meta($relatedPostId, 'time_to_read', true);
	if ( !$timeToRead ) {
		$timeToRead = 3;
	}
	$relatedTitle = get_the_title();
	$relatedAuthor = get_the_author();
	$relatedTime = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) );
	$terms_line = '';
	$relatedImageUrl = get_the_post_thumbnail_url();
	$relatedPostLink = get_post_permalink();
	$relatedContent = get_single_trimed_content(get_the_content());
	$post_taxonomy = ! is_wp_error( get_the_tags( $relatedPostId ) ) ? get_the_tags( $relatedPostId ) : array();

	if ( $post_taxonomy ) {
		foreach ( $post_taxonomy as $taxonomy => $taxonomy_value ) {
			$terms_line .= '<a href="' . get_term_link( $taxonomy_value->term_id, $tax ) . '">' . $taxonomy_value->name . '</a>';
		}
	}


	?>
<div class="widget king_news widget_news_edit_choice">
<h3 class="comment-reply-title">Читайте також</h3>
	<div class="inner widget-new-smart-inner-big-before-content read-also">
		<figure class="widget-new-smart-main">

			<div class="post-thumbnail__link">
                <a href="<?php echo $relatedPostLink; ?>">
				<img src="<?php echo $relatedImageUrl; ?>" alt="">
                    </a>
				<div class="time_to_read">
					<svg version="1.1"
						 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
						 x="0px" y="0px" width="27.6px" height="27.4px" viewBox="0 0 27.6 27.4"
						 style="overflow:scroll;enable-background:new 0 0 27.6 27.4;" xml:space="preserve">
<defs>
</defs>
						<g>
							<g>
								<path d="M26.5,9.4V7.7l-0.3-0.1c-0.1,0-2.1-0.8-4.9-0.8c-0.3,0-0.5,0-0.8,0c0-0.2,0-0.3,0-0.5c0-3.5-2.8-6.3-6.3-6.3
			C10.8,0,8,2.8,8,6.3c0,0.2,0,0.4,0,0.5C7.5,6.8,7,6.7,6.4,6.7c-2.7,0-4.8,0.8-4.9,0.8L1.3,7.7v1.6C0.5,9.4,0,9.7,0,10.2
			c0,2.3,0,16.6,0,16.6h12.2c0.4,0.4,1,0.6,1.7,0.6c0.7,0,1.3-0.2,1.7-0.6h11.9c0,0,0-15.8,0-16.6C27.6,9.8,27.2,9.5,26.5,9.4z
			 M13.8,25.4c-1-0.4-4.5-1.7-8-1.7c-1.4,0-2.6,0.2-3.7,0.6V8.2C2.7,8,4.4,7.5,6.4,7.5c2.7,0,5,0.8,6.9,2.4v9.6l0.5-0.5L13.8,25.4
			L13.8,25.4z M13.9,9.4c-1.2-1-2.6-1.8-4.1-2.2C9.8,6.9,9.7,6.6,9.7,6.3c0-2.5,2.1-4.6,4.6-4.6c1.7,0,3.3,1,4,2.4
			c0.3,0.6,0.5,1.4,0.5,2.1c0,0.3,0,0.5-0.1,0.8C17,7.4,15.3,8.2,13.9,9.4z M25.7,24.3c-1.1-0.5-2.4-0.7-3.8-0.7
			c-3.5,0-6.8,1.3-7.8,1.8V19l0.5,0.5V9.9c1.9-1.6,4.2-2.4,6.9-2.4c2,0,3.7,0.5,4.3,0.7L25.7,24.3L25.7,24.3z"/>
								<path d="M10,6.1v0.5h0.5c0-0.1,0-0.2,0-0.2c0-0.1,0-0.2,0-0.2C10.5,6.1,10,6.1,10,6.1z"/>
								<path d="M14.5,2.5V2h-0.5v0.5c0.1,0,0.2,0,0.2,0C14.4,2.5,14.5,2.5,14.5,2.5z"/>
								<path d="M18.1,6.1c0,0.1,0,0.2,0,0.2c0,0.1,0,0.2,0,0.2h0.5V6.1H18.1z"/>
								<polygon points="14.5,2.9 14.1,2.9 14.2,6.5 16.6,6.5 16.6,6.1 14.5,6.1 		"/>
							</g>
						</g>
</svg>
					<span><?php echo $timeToRead; ?></span>
                    <span class="tooltiptext">Час на читання публікації <?php echo $timeToRead; ?></span>
				</div>

				<span class="share_btn">
					<?php king_news_share_buttons( 'both-loop-single' ); ?>
				</span>
			</div>
			<figcaption >
				<div class="post__cats"><?php echo $terms_line; ?></div>



				<div class="">
					<h3 class="widget-new-smart__title">
						<a href="<?php echo $relatedPostLink; ?>">
						<?php echo $relatedTitle; ?>
						</a>
					</h3>

					<div class="widget-post-info">
						<span class="post_autor">
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"><?php echo $relatedAuthor;  ?></a>

						</span>
						<span class="post-time">
								<a href="#"><i class="material-icons">access_time</i><?php echo $relatedTime; ?> тому</a>
						</span>
					</div>
				</div>

				<div id="read-info" class="edit_choice_info">
					<?php echo $relatedContent; ?>
				</div>
			</figcaption>
		</figure>
		<div id="read-info_responsive" class="edit_choice_info_responsive">

		</div>
	</div>
	<script>
		(function () {
			var edit_choice_info = document
				.getElementById("read-info")
				.innerHTML;
			document.getElementById("read-info_responsive").innerHTML = edit_choice_info;
		})();

	</script>
</div>
	<?php

	wp_reset_postdata();



//	the_post_navigation( array(
//		'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next Post', 'king_news' ) .'</span> ' .
//										'<span class="screen-reader-text">' . __( 'Next Post', 'king_news' ) .'</span> ' .
//										'<span class="post-title">%title</span>',
//		'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous Post', 'king_news' ) .'</span> ' .
//										'<span class="screen-reader-text">' . __( 'Previous Post', 'king_news' ) .'</span> ' .
//										'<span class="post-title">%title</span>',
//	) );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
