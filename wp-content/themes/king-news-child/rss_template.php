<?php
/**
 * Template Name: RSS Template
 */
$query = new WP_Query
(array(
        'posts_per_page'   => 5,
    )
);

while ($query->have_posts()): $query->the_post(); ?>
    <li>
        <a href="<?php the_permalink(); ?>/feed"><?php the_title(); ?></a>
        <p><?php the_excerpt(); ?></p>
    </li>
<?php endwhile;

