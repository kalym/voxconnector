<?php
/**
 * Template Name: Full Page Template
 */
while ( have_posts() ) : the_post();
    the_content();
?>

<?php endwhile; ?>


<style>
    #masthead {
        display: none !important;
    }

    #colophon {
        display: none !important;
    }

    .breadcrumbs {
        display: none !important;
    }

    .site .site-content {
        padding: 0 !important;
    }

    .site-content {
        margin: 0 !important;
    }
</style>

