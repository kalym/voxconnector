<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package King_News
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header post-header">
		<?php
            king_news_meta_tags( 'single' );
			the_title( '<h1 class="entry-title">', '</h1>' );
		?>

		<?php if ( 'post' === get_post_type() ) : ?>

			<div class="entry-meta">
				<?php
					king_news_meta_author(
						'single',
						array(
							'before' => esc_html__( 'Автор:', 'king_news' ) . ' ',
						)
					);

					king_news_meta_date( 'single', array(
						'before' => '<i class="material-icons">access_time</i>',
					) );

					king_news_meta_comments( 'single', array(
						'before' => '<i class="material-icons">chat_bubble_outline</i>',
						'zero'   => esc_html__( 'Leave a comment', 'king_news' ),
						'one'    => '1',
						'plural' => '%',
					) );	?>


			<?php
			king_news_post_author_bio();
				$text = get_post_meta( get_the_ID(), 'time_to_read', true );
					if (empty($text)) { }
				else {
			?>
		<div class="time_to_read open_page">
			<svg version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
				 x="0px" y="0px" width="27.6px" height="27.4px" viewBox="0 0 27.6 27.4"
				 style="overflow:scroll;enable-background:new 0 0 27.6 27.4;" xml:space="preserve">
<defs>
</defs>
				<g>
					<g>
						<path d="M26.5,9.4V7.7l-0.3-0.1c-0.1,0-2.1-0.8-4.9-0.8c-0.3,0-0.5,0-0.8,0c0-0.2,0-0.3,0-0.5c0-3.5-2.8-6.3-6.3-6.3
			C10.8,0,8,2.8,8,6.3c0,0.2,0,0.4,0,0.5C7.5,6.8,7,6.7,6.4,6.7c-2.7,0-4.8,0.8-4.9,0.8L1.3,7.7v1.6C0.5,9.4,0,9.7,0,10.2
			c0,2.3,0,16.6,0,16.6h12.2c0.4,0.4,1,0.6,1.7,0.6c0.7,0,1.3-0.2,1.7-0.6h11.9c0,0,0-15.8,0-16.6C27.6,9.8,27.2,9.5,26.5,9.4z
			 M13.8,25.4c-1-0.4-4.5-1.7-8-1.7c-1.4,0-2.6,0.2-3.7,0.6V8.2C2.7,8,4.4,7.5,6.4,7.5c2.7,0,5,0.8,6.9,2.4v9.6l0.5-0.5L13.8,25.4
			L13.8,25.4z M13.9,9.4c-1.2-1-2.6-1.8-4.1-2.2C9.8,6.9,9.7,6.6,9.7,6.3c0-2.5,2.1-4.6,4.6-4.6c1.7,0,3.3,1,4,2.4
			c0.3,0.6,0.5,1.4,0.5,2.1c0,0.3,0,0.5-0.1,0.8C17,7.4,15.3,8.2,13.9,9.4z M25.7,24.3c-1.1-0.5-2.4-0.7-3.8-0.7
			c-3.5,0-6.8,1.3-7.8,1.8V19l0.5,0.5V9.9c1.9-1.6,4.2-2.4,6.9-2.4c2,0,3.7,0.5,4.3,0.7L25.7,24.3L25.7,24.3z"/>
						<path d="M10,6.1v0.5h0.5c0-0.1,0-0.2,0-0.2c0-0.1,0-0.2,0-0.2C10.5,6.1,10,6.1,10,6.1z"/>
						<path d="M14.5,2.5V2h-0.5v0.5c0.1,0,0.2,0,0.2,0C14.4,2.5,14.5,2.5,14.5,2.5z"/>
						<path d="M18.1,6.1c0,0.1,0,0.2,0,0.2c0,0.1,0,0.2,0,0.2h0.5V6.1H18.1z"/>
						<polygon points="14.5,2.9 14.1,2.9 14.2,6.5 16.6,6.5 16.6,6.1 14.5,6.1 		"/>
					</g>
				</g>
</svg>
			<?php	echo esc_html($text);  ?>
			<span></span>
			<span class="tooltiptext">Час на читання публікації <?php echo esc_html($text); ?></span>
		</div>
			<?php } ?>
			</div><!-- .entry-meta -->
		<?php endif; ?>

	</header><!-- .entry-header -->

	<figure class="post-thumbnail">
		<?php king_news_post_thumbnail( false ); ?>
	</figure><!-- .post-thumbnail -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'king_news' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
<!--		--><?php
//			king_news_meta_tags( 'single', array(
//				'before'    => '<i class="material-icons">folder_open</i>Tagged in: ',
//				'separator' => ', ',
//			) );
//		?>
		<div class="reservation">
			<p class="reservation-title">Застереження</p>
			<p class="reservation_description"><?php echo get_theme_mod( 'text_setting' );?></p>
		</div>
	</footer><!-- .entry-footer -->



	<?php
	 	wp_reset_postdata();
	?>

</article><!-- #post-## -->


