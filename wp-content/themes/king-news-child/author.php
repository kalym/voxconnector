<?php
/**
 * Template Name: Author Template Description
 */
?>
<?php
$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
?>
<div class="author-page">
    <div class="single-author-name">
        <h1 class="entry-title author-page-title"><?php echo $curauth->display_name; ?>  </h1>
    </div>
    <div class="profile-picture">
            <?php  echo get_avatar(get_the_author_meta('ID'), 150); ?>
        </div>
           <a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a>
    <div class="author-page-content">
        <div class="primary_info">
            <p><?php the_author_meta( 'information', $user_id ); ?></p>
        </div>
        <div class="secondary_info"><?php echo $curauth->user_description; ?></div>
    </div>
</div>

<aside class="widget widget-image-grid author-post">
    <div class="row image_grid_widget-main columns-number-3" style="margin-left:-5px">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="widget-image-grid__holder invert col-xs-12 col-md-12 col-lg-6 col-xl-4">
            <figure class="widget-image-grid__inner" style="margin: 0 0 5px 5px;">
            <div class="author_post_img_wrap"><?php echo the_post_thumbnail();?></div>
                <figcaption class="widget-image-grid__content">
                    <div class="post__cats"><?php the_category('');?></div>
                    <div class="widget-image-grid__content-2">
                        <h3 class="widget-image-grid__title">
                             <a href="<?php echo get_permalink(); ?>"> <?php echo get_the_title(); ?> </a>
                        </h3>
                        <div class="widget-new-smart__footer-2 ">
					<span class="widget-new-smart__post__date">
						<?php
                        king_news_meta_date( 'loop', array(
                            'before' => '<i class="material-icons">access_time</i>',
                        ) );
                        ?>
					</span>

                            <?php king_news_share_buttons( 'loop' ); ?>

                        </div>
                    </div>
                </figcaption>
            </figure>
        </div>   <?php endwhile; endif; ?>
        </div>
    </aside>