<?php
/**
 * The template for displaying author bio.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package king_news
 */

global $post;
if (count(get_coauthors()) == 1)
{?>
<div class="post-author-bio invert">
	<div class="post-author__holder clear">
		<div class="post-author__avatar"><?php
			echo get_avatar( get_the_author_meta( 'user_email' ), 140, '', esc_attr( get_the_author_meta( 'nickname' ) ) );
		?></div>
		<h4 class="post-author__title"><?php
			printf( esc_html__( 'Written by %s', 'king_news' ), king_news_get_the_author_posts_link() );
		?></h4>
		<div class="post-author__content"><?php
			echo get_the_author_meta( 'description' );
		?></div>
	</div>
</div>
	<?php }
else{ ?>
	<div class="post-author-bio invert">
		<div class="post-author__holder clear">
			<h4 class="post-author__title align_center">Автори</h4>
			<?php 				foreach( get_coauthors() as $coauthor ): ?>
				<div class="width_100">
			<div class="post-author__avatar width_35"><?php
				echo get_avatar( $coauthor->user_email, '96' );
				?></div>
			<h4 class="post-author__title  width_65"><?php
				printf(esc_html__('%s','king_news'),'<a class="author url fn" href="'.get_author_posts_url($coauthor->ID,$coauthor->user_nicename).'">'.$coauthor->display_name.'</a>');
				?></h4>
			<div class="post-author__content width_65"><?php
				echo $coauthor->description;
				?></div>
				</div>
	<?php 	endforeach;  ?>
		</div>
	</div>
<?php  } ?>
