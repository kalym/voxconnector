<div class="widget-image-grid__holder widget_third_display_none invert col-xs-12 col-md-<?php echo 12?> col-lg-6 col-xl-<?php echo $columns_class; ?>">
	<figure class="widget-image-grid__inner" <?php echo $inline_style; ?>>
        <div class="widget-img-wrap">
            <a href="<?php echo $permalink; ?>">
		    <?php echo $image; ?>
            </a>
        </div>
		<figcaption class="widget-image-grid__content">

      <?php  echo $termsLine;

      ?>

            <div class="time_to_read">
                <svg version="1.1"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
                     x="0px" y="0px" width="27.6px" height="27.4px" viewBox="0 0 27.6 27.4"
                     style="overflow:scroll;enable-background:new 0 0 27.6 27.4;" xml:space="preserve">
<defs>
</defs>
                    <g>
                        <g>
                            <path d="M26.5,9.4V7.7l-0.3-0.1c-0.1,0-2.1-0.8-4.9-0.8c-0.3,0-0.5,0-0.8,0c0-0.2,0-0.3,0-0.5c0-3.5-2.8-6.3-6.3-6.3
			C10.8,0,8,2.8,8,6.3c0,0.2,0,0.4,0,0.5C7.5,6.8,7,6.7,6.4,6.7c-2.7,0-4.8,0.8-4.9,0.8L1.3,7.7v1.6C0.5,9.4,0,9.7,0,10.2
			c0,2.3,0,16.6,0,16.6h12.2c0.4,0.4,1,0.6,1.7,0.6c0.7,0,1.3-0.2,1.7-0.6h11.9c0,0,0-15.8,0-16.6C27.6,9.8,27.2,9.5,26.5,9.4z
			 M13.8,25.4c-1-0.4-4.5-1.7-8-1.7c-1.4,0-2.6,0.2-3.7,0.6V8.2C2.7,8,4.4,7.5,6.4,7.5c2.7,0,5,0.8,6.9,2.4v9.6l0.5-0.5L13.8,25.4
			L13.8,25.4z M13.9,9.4c-1.2-1-2.6-1.8-4.1-2.2C9.8,6.9,9.7,6.6,9.7,6.3c0-2.5,2.1-4.6,4.6-4.6c1.7,0,3.3,1,4,2.4
			c0.3,0.6,0.5,1.4,0.5,2.1c0,0.3,0,0.5-0.1,0.8C17,7.4,15.3,8.2,13.9,9.4z M25.7,24.3c-1.1-0.5-2.4-0.7-3.8-0.7
			c-3.5,0-6.8,1.3-7.8,1.8V19l0.5,0.5V9.9c1.9-1.6,4.2-2.4,6.9-2.4c2,0,3.7,0.5,4.3,0.7L25.7,24.3L25.7,24.3z"/>
                            <path d="M10,6.1v0.5h0.5c0-0.1,0-0.2,0-0.2c0-0.1,0-0.2,0-0.2C10.5,6.1,10,6.1,10,6.1z"/>
                            <path d="M14.5,2.5V2h-0.5v0.5c0.1,0,0.2,0,0.2,0C14.4,2.5,14.5,2.5,14.5,2.5z"/>
                            <path d="M18.1,6.1c0,0.1,0,0.2,0,0.2c0,0.1,0,0.2,0,0.2h0.5V6.1H18.1z"/>
                            <polygon points="14.5,2.9 14.1,2.9 14.2,6.5 16.6,6.5 16.6,6.1 14.5,6.1 		"/>
                        </g>
                    </g>
</svg><span><?php echo $readTime; ?></span>
                <span class="tooltiptext">Час на читання публікації <?php echo $readTime; ?></span>
            </div>

			<div class="widget-image-grid__content-2">

        <h3 class="widget-image-grid__title">
          <a href="<?php echo $permalink; ?>"><?php echo $title ?></a>
        </h3>

        <div class="widget-image-grid__footer">
          <div class="widget-image-grid__footer-meta">
            <a class="widget-image-grid__link focus_time_publicate" href="<?php echo $permalink; ?>"><i class="material-icons dp18">access_time</i><?php echo $date; ?></a>
          </div>
                <div class="focus_author_hover">  <?php
                    if (count(get_coauthors()) == 1){
                        foreach( get_coauthors() as $authors ) {
                            echo '<a href="'.get_author_posts_url($authors->ID,$authors->user_nicename).'">'.$authors->display_name.' </a>';
                        }
                    }
                    else{
                        foreach( get_coauthors() as $authors ) {
                            echo '<a href="'.get_author_posts_url($authors->ID,$authors->user_nicename).'">'.$authors->display_name.' </a> <span>та</span> ';
                        }
                    }  ?> </div>
                <?php king_news_share_buttons( 'both-loop-single' ); ?>
        </div>
      </div>


    </figcaption>
  </figure>
</div>