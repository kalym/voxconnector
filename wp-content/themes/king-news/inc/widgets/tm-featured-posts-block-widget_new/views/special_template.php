<?php
/**
 * Template part to display a single post while in a layout posts loop
 *
 * @package    __tm/widgets
 */


//global $post;

$image_url = get_the_post_thumbnail_url( $banner_post, 'original' );
$post_content = $banner_post->post_content;
$post_title = $banner_post->post_title;
$post_link =  get_permalink($banner_post);
?>
<div class="top_news_block_wrap">
    <div class="top_news_block">
        <a class="wrap_link" href="<?php echo $post_link; ?>">
        </a>
        <img src="<?php echo $image_url; ?>" alt="img">

        <a class="title" href="<?php echo $post_link; ?>">
            <h3><?php echo $post_title; ?></h3>
        </a>
    </div>
</div>
