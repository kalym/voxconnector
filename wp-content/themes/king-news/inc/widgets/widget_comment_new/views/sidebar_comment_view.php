<div class="inner">

	<div class="entry-content">
		<h5 class="widget-new-smart__title widget-new-smart__title-small">
			<?php echo $title; ?>
		</h5>

		<div class="entry-meta">
			<div class="meta-inner meta_inner_comment">
				<span class="post__date">
					<?php
						king_news_meta_date( 'loop', array(
							'before' => '<i class="material-icons">access_time</i>',
						) );
					 ?>
					<span class="smart_box_mini_author">
					<?php echo $author; ?>
					</span>
				</span>
			</div>

			<?php king_news_share_buttons( 'loop' ); ?>
		</div>
	</div>
	<footer class="entry-footer"></footer>
</div>