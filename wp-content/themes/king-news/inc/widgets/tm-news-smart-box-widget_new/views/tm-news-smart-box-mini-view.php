<div class="inner">

	<div class="entry-content">
		<h5 class="widget-new-smart__title widget-new-smart__title-small">
			<?php echo $title; ?>
		</h5>
		<?php echo $content; ?>
		<div class="entry-meta">
			<div class="meta-inner">
				<span class="post__date">
					<?php
						king_news_meta_date( 'loop', array(
							'before' => '<i class="material-icons">access_time</i>',
						) );
					 ?>
					<span class="smart_box_mini_author">

					<?php
					if (count(get_coauthors()) == 1){
						foreach( get_coauthors() as $authors ) {
							echo '<a href="'.get_author_posts_url($authors->ID,$authors->user_nicename).'">'.$authors->display_name.' </a>';
						}
					}
					else{
						foreach( get_coauthors() as $authors ) {
							echo '<a href="'.get_author_posts_url($authors->ID,$authors->user_nicename).'">'.$authors->display_name.' </a> <span>та</span> ';
						}
					} ?>
					</span>
				</span>

				<?php
					king_news_meta_comments( 'loop', array(
						'before' => '<i class="material-icons">chat_bubble_outline</i>',
						'after' => '',
						'zero'   => esc_html__( 'Leave a comment', 'king_news' ),
						'one'    => '1 comment',
						'plural' => '% comments',
					) );
				?>

			</div>

			<?php king_news_share_buttons( 'loop' ); ?>
		</div>
	</div>
	<footer class="entry-footer"></footer>
</div>