jQuery(document).ready(function () {
    var userList = new List('users', {
    valueNames: [ 'name'],
    page: 9,
    pagination: true,
    item: 'name'
});

    jQuery('#updateList').on('click', function() {
        userList['page'] = '9999';
        userList['i'] = '1';
        jQuery('#users .pagination').css('display', 'none');
        jQuery(this).css('display', 'none');
        userList.update();
    });

userList.on('updated', function(list) {
    if (list.matchingItems.length > 0) {
        jQuery('.no-result').hide()
    } else {
        jQuery('.no-result').show()
    }
    
});

var search_link = jQuery('.alphabet_search').find('a');
var input_val = jQuery('.search_author').find('.search');

search_link.on('click', function (event) {
    event.preventDefault();
    search_link.removeClass('selected');
    jQuery(this).addClass('selected');
    var letter = jQuery(this).text();
    userList.search(letter, ['name']);
    jQuery('.list').removeHighlight();
    jQuery('.list').highlight(letter);
    input_val.val(letter);
});

input_val.keyup(function() {
    var search_val = jQuery(this).val();
    jQuery('.list').removeHighlight();
    jQuery('.list').highlight(search_val);
    jQuery(search_link).removeClass('selected');
});
    jQuery(window).scroll(function () {
        if (jQuery(document).scrollTop() > 0) {
            jQuery('.header-container__flex').addClass('scrolled');
            jQuery('.header-container_inner').addClass('scrolled');
        } else {
            jQuery('.header-container__flex').removeClass('scrolled');
            jQuery('.header-container_inner').removeClass('scrolled');
        }
    });

});